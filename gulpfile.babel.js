/**
 *
 *  Web Starter Kit
 *  Copyright 2015 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

'use strict';

// This gulpfile makes use of new JavaScript features.
// Babel handles this without us having to do anything. It just works.
// You can read more about the new JavaScript features here:
// https://babeljs.io/docs/learn-es2015/

import path from 'path';
import gulp from 'gulp';
import del from 'del';
import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
import swPrecache from 'sw-precache';
import gulpLoadPlugins from 'gulp-load-plugins';
import {output as pagespeed} from 'psi';
import pkg from './package.json';
import {spawn} from 'child_process';
import hugoBin from 'hugo-bin';

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const assetsDir = 'src/assets';
const siteDir = 'src/site';

// Hugo arguments
const hugoArgsDefault = ['-d', '../../.tmp', '-s', siteDir, '-v'];
const hugoArgsPreview = ['--buildDrafts', '--buildFuture'];

// Lint JavaScript
gulp.task('lint', () =>
  gulp.src([`${assetsDir}/scripts/**/*.js`, '!node_modules/**'])
    .pipe($.newer('.tmp/scripts'))
    .pipe($.eslint({fix: true}))
    .pipe(gulp.dest(`${assetsDir}/scripts`))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()))
);

// Optimize images
gulp.task('images', () =>
  gulp.src(`${assetsDir}/images/**/*`)
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'))
    .pipe($.size({title: 'images'}))
);

// Copy all files at the root level (src/assets)
gulp.task('copy', () =>
  gulp.src([
    `${assetsDir}/*`,
    `!${assetsDir}/*.html`,
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'))
    .pipe($.size({title: 'copy'}))
);

// Compile and automatically prefix stylesheets
gulp.task('styles:dev', () => {
  // For best performance, don't add Sass partials to `gulp.src`
  return gulp.src([
    `${assetsDir}/styles/**/*.scss`,
    `${assetsDir}/styles/**/*.css`
  ])
    .pipe($.plumber())
    .pipe($.newer('.tmp/styles'))
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      outputStyle: 'expanded',
      precision: 10
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer())
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream: true}));
});

// Compile and automatically prefix stylesheets
gulp.task('styles', () => {
  // For best performance, don't add Sass partials to `gulp.src`
  return gulp.src([
    `${assetsDir}/styles/**/*.scss`,
    `${assetsDir}/styles/**/*.css`
  ])
    .pipe($.plumber())
    .pipe($.sass({
      outputStyle: 'compact',
      precision: 10
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer())
    // Concatenate and minify styles
    .pipe($.cssnano({safe: true, autoprefixer: false}))
    .pipe(gulp.dest('dist/styles'))
    .pipe($.size({title: 'styles'}));
});

// Optionally transpiles ES2015 code to ES5. To enable ES2015 support remove the
// line `"only": "gulpfile.babel.js",` in the `.babelrc` file.
gulp.task('scripts:dev', ['lint'], () =>
  gulp.src(`./${assetsDir}/scripts/**/*.js`)
    .pipe($.plumber())
    .pipe($.newer('.tmp/scripts'))
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp/scripts'))
);

// Concatenate and minify JavaScript. Optionally transpiles ES2015 code to ES5.
// To enable ES2015 support remove the line `"only": "gulpfile.babel.js",` in the
// `.babelrc` file.
gulp.task('scripts', ['lint'], () =>
  gulp.src([
    // Note: Since we are not using useref in the scripts build pipeline,
    //       you need to explicitly list your scripts here in the right order
    //       to be correctly concatenated
    `./${assetsDir}/scripts/main.js`
    // Other scripts
  ])
    .pipe($.plumber())
    .pipe($.babel())
    .pipe($.concat('main.min.js'))
    .pipe($.uglify({
      compress: {drop_console: true},
      output: {comments: 'some'}
    }))
    // Output files
    .pipe(gulp.dest('dist/scripts'))
    .pipe($.size({title: 'scripts'}))
);

// Scan your HTML for assets & optimize them
gulp.task('html', () => {
  return gulp.src(`${assetsDir}/**/*.html`)
    .pipe($.useref({
      searchPath: `{.tmp,${assetsDir}}`,
      noAssets: true
    }))
    // Minify any HTML
    .pipe($.if('*.html', $.htmlmin({
      collapseBooleanAttributes: true,
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: {compress: {drop_console: true}},
      processConditionalComments: true,
      removeAttributeQuotes: true,
      removeComments: true,
      removeEmptyAttributes: true,
      removeOptionalTags: true,
      removeRedundantAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    })))
    // Output files
    .pipe(gulp.dest('dist'))
    .pipe($.if('*.html', $.size({title: 'html', showFiles: true})));
});

// Clean output directory
gulp.task('clean', () => del(['.tmp', 'dist/*', '!dist/.git'], {dot: true}));

// Watch files for changes & reload
gulp.task('serve', ['hugo', 'scripts:dev', 'styles:dev'], () => {
  browserSync({
    notify: false,
    // Customize the Browsersync console logging prefix
    logPrefix: 'WSK',
    // Allow scroll syncing across breakpoints
    scrollElementMapping: ['main', '.mdl-layout'],
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server: ['.tmp', assetsDir],
    port: 3000
  });

  gulp.watch([`${siteDir}/**/*`], ['hugo', reload]);
  gulp.watch([`${assetsDir}/styles/**/*.{scss,css}`], ['styles:dev']);
  gulp.watch([`${assetsDir}/scripts/**/*.js`], ['scripts:dev', reload]);
  gulp.watch([`${assetsDir}/images/**/*`], reload);
});

// Watch files for changes & reload
gulp.task('serve:preview', ['hugo:preview', 'scripts:dev', 'styles:dev'], () => {
  browserSync({
    notify: false,
    // Customize the Browsersync console logging prefix
    logPrefix: 'WSK',
    // Allow scroll syncing across breakpoints
    scrollElementMapping: ['main', '.mdl-layout'],
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server: ['.tmp', assetsDir],
    port: 3000
  });

  gulp.watch([`${siteDir}/**/*`], ['hugo:preview', reload]);
  gulp.watch([`${assetsDir}/styles/**/*.{scss,css}`], ['styles:dev']);
  gulp.watch([`${assetsDir}/scripts/**/*.js`], ['scripts:dev', reload]);
  gulp.watch([`${assetsDir}/images/**/*`], reload);
});

// Build and serve the output from the dist build
gulp.task('serve:dist', ['default'], () =>
  browserSync({
    notify: false,
    logPrefix: 'WSK',
    // Allow scroll syncing across breakpoints
    scrollElementMapping: ['main', '.mdl-layout'],
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server: 'dist',
    port: 3001
  })
);

gulp.task('size', () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

// Build production files, the default task
gulp.task('default', ['clean'], cb =>
  runSequence(
    'styles',
    ['html', 'scripts', 'images', 'copy'],
    'generate-service-worker',
    'size',
    cb
  )
);

gulp.task('build:preview', cb =>
  runSequence(
    'default',
    cb
  )
);

// Development tasks
gulp.task('hugo', cb => buildSite(cb));
gulp.task('hugo:preview', cb => buildSite(cb, hugoArgsPreview));

// Run hugo and build the site
function buildSite(cb, options, environment = 'development') {
  const args = options ? hugoArgsDefault.concat(options) : hugoArgsDefault;

  process.env.NODE_ENV = environment;

  return spawn(hugoBin, args, {stdio: 'inherit'}).on('close', code => {
    if (code === 0) {
      browserSync.reload();
      cb();
    } else {
      browserSync.notify('Hugo build failed :(');
      cb('Hugo build failed');
    }
  });
}

// Run PageSpeed Insights
gulp.task('pagespeed', cb =>
  // Update the below URL to the public URL of your site
  pagespeed('example.com', {
    strategy: 'mobile'
    // By default we use the PageSpeed Insights free (no API key) tier.
    // Use a Google Developer API key if you have one: http://goo.gl/RkN0vE
    // key: 'YOUR_API_KEY'
  }, cb)
);

// Copy over the scripts that are used in importScripts as part of the generate-service-worker task.
gulp.task('copy-sw-scripts', () => {
  return gulp.src(['node_modules/sw-toolbox/sw-toolbox.js', `${assetsDir}/scripts/sw/runtime-caching.js`])
    .pipe(gulp.dest('dist/scripts/sw'));
});

// See http://www.html5rocks.com/en/tutorials/service-worker/introduction/ for
// an in-depth explanation of what service workers are and why you should care.
// Generate a service worker file that will provide offline functionality for
// local resources. This should only be done for the 'dist' directory, to allow
// live reload to work as expected when serving from '.tmp', 'src/assets'
// directories.
gulp.task('generate-service-worker', ['copy-sw-scripts'], () => {
  const rootDir = 'dist';
  const filepath = path.join(rootDir, 'service-worker.js');

  return swPrecache.write(filepath, {
    // Used to avoid cache conflicts when serving on localhost.
    cacheId: pkg.name || 'web-starter-kit',
    // sw-toolbox.js needs to be listed first. It sets up methods used in runtime-caching.js.
    importScripts: [
      'scripts/sw/sw-toolbox.js',
      'scripts/sw/runtime-caching.js'
    ],
    staticFileGlobs: [
      // Add/remove glob patterns to match your directory setup.
      `${rootDir}/images/**/*`,
      `${rootDir}/scripts/**/*.js`,
      `${rootDir}/styles/**/*.css`,
      `${rootDir}/**/*.{html,json}`
    ],
    // Translates a static file path to the relative URL that it's served from.
    // This is '/' rather than path.sep because the paths returned from
    // glob always use '/'.
    stripPrefix: rootDir + '/'
  });
});

// Load custom tasks from the `tasks` directory
// Run: `npm install --save-dev require-dir` from the command-line
// try { require('require-dir')('tasks'); } catch (err) { console.error(err); }
